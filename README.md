# Projet de Rapports Statistiques pour Climax

Ce projet vise à développer une application permettant de générer des rapports statistiques sur les clients de la société Climax, en se basant sur les données fournies par les partenaires sous différents formats de fichiers (texte, CSV, XML, JSON).

## Technologies utilisées

- Java : Backend de l'application, utilisant Spring Boot.
- Angular : Frontend de l'application.
- Docker : Pour le déploiement et la gestion des conteneurs.
- Bibliothèques Java :
    - [Apache Commons IO](https://commons.apache.org/proper/commons-io/) : Lecture de fichiers texte.
    - [OpenCSV](http://opencsv.sourceforge.net/) : Lecture de fichiers CSV.
    - [Jackson](https://github.com/FasterXML/jackson) : Lecture de fichiers JSON.
    - [JAXB](https://jaxb.java.net/) : Lecture de fichiers XML.

## Installation et exécution

### Backend (Spring Boot)

1. Clonez ce dépôt : `git clone git@gitlab.com:badourobert.soulama/climaxstat-backend.git`
2. Naviguez vers le répertoire backend : `cd backend`
3. Ouvrir avec IntelliJ IDE et Exécutez l'application Spring Boot

Le serveur backend démarrera sur `http://localhost:8080`.

### Frontend (Angular)

1. Clonez ce dépôt : `git clone git@gitlab.com:badourobert.soulama/climaxstat-front.git`
2. Naviguez vers le répertoire frontend : `cd frontend`
3. Installez les dépendances : `npm install`
4. Démarrez l'application Angular : `ng serve`

Le serveur frontend sera accessible sur `http://localhost:4200`.

## Utilisation

1. Importez les fichiers de données via l'interface utilisateur.
2. Consultez la moyenne des salaires par type de profession en bas du champ de chargement du fichier.

## Docker

Vous pouvez également utiliser Docker pour exécuter l'application dans un conteneur.

1. Construisez l'image Docker : `docker build -t climax-stat-front .`
2. Exécutez le conteneur Docker : `docker run -p 80:80 climax-stat-front`

L'application sera disponible sur `http://localhost:80`.

## Contributions

Les contributions sont les bienvenues ! Si vous souhaitez contribuer à ce projet, veuillez ouvrir une issue pour discuter des changements proposés.

## Auteurs

Ce projet a été réalisé par [Badou Robert Soulama](mailto:badourobert.soulama@gmail.com).

## Licence

Ce projet est sous licence MIT. Consultez le fichier `LICENSE` pour plus de détails.