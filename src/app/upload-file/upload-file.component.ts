import { Component } from '@angular/core';
import { UploadFileService } from '../upload-file.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-upload-file',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './upload-file.component.html',
  styleUrl: './upload-file.component.scss'
})
export class UploadFileComponent {

  selectedFile: File | null = null;
results: any[] = [];

  constructor(private uploadFileService: UploadFileService){}

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  uploadFile() {
    if (this.selectedFile) {
      this.uploadFileService.uploadFile(this.selectedFile).subscribe({
        next : (response : any) => {
          console.log('Fichier chargé avec succes');
          this.results = Object.entries(response).map(([profession, averageSalary]) => ({
            profession,
            averageSalary
          }));
        },
        error: (error: any) => {
          console.error('Erreur lors du chargemnt du fichier', error);
        }
      }
      );
    }
  }

}
